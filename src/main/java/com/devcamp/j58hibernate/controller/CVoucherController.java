package com.devcamp.j58hibernate.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j58hibernate.model.CVoucher;
import com.devcamp.j58hibernate.repository.IVoucherRespository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CVoucherController {
   @Autowired
   IVoucherRespository voucherRep;

   @GetMapping("/voucher")
   public ResponseEntity<List<CVoucher>> getVouchergetVoucherList() {
       try {
           List<CVoucher>  list = new ArrayList<CVoucher>();
           voucherRep.findAll().forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null;
       }
      
   }
   @GetMapping("/voucher5")
   public ResponseEntity<List<CVoucher>> getFiveVoucher(
                       @RequestParam(value = "page", defaultValue = "1") String page,
                       @RequestParam(value = "size", defaultValue = "5") String size) {
       try {
           Pageable pageWithFiveElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));
           List<CVoucher>  list = new ArrayList<CVoucher>();
           voucherRep.findAll(pageWithFiveElements).forEach(list::add);
           return new ResponseEntity<>(list,HttpStatus.OK);
       } catch (Exception e) {
           return null;
       }
   } 
}
