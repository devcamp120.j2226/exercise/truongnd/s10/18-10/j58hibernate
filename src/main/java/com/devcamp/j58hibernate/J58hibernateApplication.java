package com.devcamp.j58hibernate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J58hibernateApplication {

	public static void main(String[] args) {
		SpringApplication.run(J58hibernateApplication.class, args);
	}

}
